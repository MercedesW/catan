from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView, status
from rest_framework import permissions
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User


class UserRegister(APIView):

    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        try:
            user_field = request.data["user"]
            pass_field = request.data["pass"]
        except KeyError:
            return Response("Error de escritura", status=status.HTTP_400_BAD_REQUEST)

        if User.objects.filter(username=user_field).count() >= 1:
            return Response("El usuario ya está registrado", status=status.HTTP_400_BAD_REQUEST)

        User.objects.create_user(username=user_field, password=pass_field)
        return Response("Usuario creado", status=status.HTTP_201_CREATED)


class UserLogin(APIView):

    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        try:
            user_field = request.data["user"]
            pass_field = request.data["pass"]
        except KeyError:
            return Response("Error de escritura", status=status.HTTP_400_BAD_REQUEST)

        user = get_object_or_404(User, username=user_field)
        if not user.check_password(pass_field):
            return Response("El usuario no está registrado", status=status.HTTP_401_UNAUTHORIZED)

        try:
            token = Token.objects.get(user=user)
        except Token.DoesNotExist:
            token = Token.objects.create(user=user)

        return Response({"token": token.key})
