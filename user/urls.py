from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from user import views

urlpatterns = [
    path('users/', views.UserRegister.as_view()),
    path('users/login/', views.UserLogin.as_view()),
    path('users', views.UserRegister.as_view()),
    path('users/login', views.UserLogin.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)