from django.db import models
from room.models import Room


class Game(models.Model):
    dice1 = models.IntegerField(default=0)
    dice2 = models.IntegerField(default=0)
    robber_hex_level = models.IntegerField(default=-1)
    robber_hex_index = models.IntegerField(default=-1)
    room = models.OneToOneField(
        Room,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    def __str__(self):
        return "%s the game" % self.room.name


def create_game(room):
    room.game_has_started = True
    room.game_id = room.id
    room.save()

    game = Game(room=room)
    game.save()
    return game
