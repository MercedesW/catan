from django.db import models


class Board(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return "%s the board" % self.name
