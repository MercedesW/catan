from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from django.contrib.auth.models import User
from board.models import Board


class getTableroTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='Toshi',
                                             password='Come33veces!')
        self.board = Board.objects.create(name="Reedwind")
        self.board2 = Board.objects.create(name="Exeloch")

        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_get_boards_list_endpoint(self):
        response = self.client.get('/boards/')
        assert response.status_code == 200

    def test_get_size_of_expected_board_list(self):
        response = self.client.get('/boards/')
        self.assertEqual(len(response.data), 2)
