from django.db import models
from catan.game_elements import PlayerColour
from game.models import Game


class Player(models.Model):
    user = models.CharField(max_length=50)
    colour = models.CharField(max_length=1, choices=PlayerColour.COLOUR)
    game = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    # inTurn = models.BooleanField(default=False)
    # winner = models.BooleanField(default=False)
    # victoryPoint = models.IntegerField(default=0)
    # lastGained = models.CharField(null=True, max_length=1)

    def __str__(self):
        return "%s the player" % self.user.username


def create_players(room, user, game):
    colors = [k[1] for k in PlayerColour.COLOUR]

    for user in room.players.all():
        player = Player.objects.create(user=user,
                                       colour=colors[0],
                                       game=game)
        colors.pop(0)
