from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from catan.game_elements import PlayerColour
from room.models import Room, build_room_model, validate_room, room_exists
from room.serializers import RoomSerializer
from board.models import Board
from game.models import Game, create_game
from player.models import Player, create_players


class RoomListCreate(APIView):

    def get(self, request, *args, **kwargs):

        room_model = Room.objects.all()
        serializer = RoomSerializer(room_model, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK,)

    def post(self, request):

        room_model = build_room_model(request)
        serializer = RoomSerializer(room_model)
        return Response(serializer.data, status=status.HTTP_201_CREATED,)


class RoomDetail(APIView):

    def get(self, request, pk, format=None):

        room = room_exists(pk)
        serializer = RoomSerializer(room)
        return Response(serializer.data)

    def put(self, request, pk):
        user = request.user
        room = room_exists(pk)

        if not validate_room(room, user):
            return Response(status=status.HTTP_409_CONFLICT)

        room.players.add(user)
        room.save()
        return Response("Se ha unido exitosamente",
                        status=status.HTTP_202_ACCEPTED)

    def delete(self, request, pk, format=None):
        user = request.user
        room = room_exists(pk)
        owner = room.owner

        if user_has_not_permission(owner, user):
            return Response("No tiene permisos para borrar la sala",
                            status=status.HTTP_401_UNAUTHORIZED)
        if game_has_started(room):
            return Response("El juego ya empezó",
                            status=status.HTTP_409_CONFLICT)

        room.delete()
        return Response("Juego borrado", status=status.HTTP_204_NO_CONTENT)

    def patch(self, request, pk):
        user = request.user
        room = room_exists(pk)
        owner = room.owner

        if user_has_not_permission(owner, user):
            return Response("Sólo el dueño crea el juego",
                            status=status.HTTP_401_UNAUTHORIZED)

        if game_can_not_start(room):
            return Response(status=status.HTTP_409_CONFLICT)

        game = create_game(room)
        create_players(room, user, game)
        return Response("Partida iniciada", status=status.HTTP_202_ACCEPTED)


# Buscar donde poner esto.


def game_can_not_start(room):
    invalid_game = (number_players(room) not in [3, 4] or
                    game_has_started(room))
    return invalid_game


def user_has_not_permission(owner, user):
    permission = owner != user
    return permission


def game_has_started(room):
    return room.game_has_started


def number_players(room):
    return room.players.all().count()
