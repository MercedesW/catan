from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from django.contrib.auth.models import User
from board.models import Board
from room.models import Room


class RoomsListCreateTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='Toshi',
                                             password='Come33veces!')
        self.board = Board.objects.create(name="Reedwind")
        self.room = Room.objects.create(name='The Dragon room',
                                        board=self.board)
        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_get_room_list_endpoint(self):
        response = self.client.get('/rooms/')
        assert response.status_code == 200

    def test_get_size_of_expected_room_list(self):
        response = self.client.get('/rooms/')
        self.assertEqual(len(response.data), 1)

    def test_create_room_endpoint(self):
        data = {'name': 'Caballeros Rojos', 'board_id': self.board.id}
        response = self.client.post('/rooms/', data)
        assert response.status_code == 201

    def test_create_correct_content_room(self):
        data = {'name': 'Caballeros Rojos', 'board_id': self.board.id}
        response = self.client.post('/rooms/', data)

        self.assertEqual(response.data["name"], 'Caballeros Rojos')
        self.assertEqual(response.data["owner"], self.user.username)
        self.assertEqual(response.data["players"], [self.user.username])

    def test_get_one_room(self):
        response = self.client.get('/rooms/1/')
        assert response.status_code == 200
        assert Room.objects.filter(id=self.room.id).count() == 1

    def test_get_one_room_does_not_exist(self):
        response = self.client.get('/rooms/13/')
        assert response.status_code == 404
        assert not Room.objects.filter(id=13).exists()


class RoomsJoinTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='Toshi',
                                             password='Come33veces!')
        self.user2 = User.objects.create_user(username='Pote',
                                              password='Come33veces!')
        self.user3 = User.objects.create_user(username='Hercules',
                                              password='Come33veces!')
        self.user4 = User.objects.create_user(username='Ernesto',
                                              password='Come33veces!')
        self.user5 = User.objects.create_user(username='Lucy',
                                              password='Come33veces!')
        self.board = Board.objects.create(name="Reedwind")
        self.room = Room.objects.create(name='The Dragon room',
                                        owner=self.user,
                                        game_has_started=False,
                                        board=self.board)
        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_join_user_to_room(self):
        self.token = Token.objects.create(user=self.user2)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.put('/rooms/' + str(self.room.id) + '/')
        assert self.room.players.count() == 1
        assert response.status_code == 202

    def test_join_user_to_game_has_started(self):
        self.room.game_has_started = True
        self.room.save()
        self.token = Token.objects.create(user=self.user2)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.put('/rooms/' + str(self.room.id) + '/')
        assert self.room.players.count() == 0
        assert response.status_code == 409

    def test_join_fifth_user_to_room(self):
        users = [self.user, self.user4, self.user3, self.user2]
        for user in users:
            self.room.players.add(user)

        self.token = Token.objects.create(user=self.user5)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.put('/rooms/' + str(self.room.id) + '/')
        assert self.room.players.count() == 4
        assert response.status_code == 409

    def test_join_user_already_exists_to_room(self):
        users = [self.user, self.user4, self.user3]
        for user in users:
            self.room.players.add(user)

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.put('/rooms/' + str(self.room.id) + '/')
        assert self.room.players.count() == 3
        assert response.status_code == 409


class RoomsDeleteTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='Toshi',
                                             password='Come33veces!')
        self.user2 = User.objects.create_user(username='Hercules',
                                              password='Come33veces!')
        self.board = Board.objects.create(name="Reedwind")
        self.room = Room.objects.create(name='The Dragon room',
                                        game_has_started=False,
                                        owner=self.user,
                                        board=self.board)
        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_delete_room_endpoint(self):
        response = self.client.delete('/rooms/1/')
        assert response.status_code == 204
        assert not Room.objects.filter(id=1).exists()

    def test_delete_room_not_owner(self):
        self.token = Token.objects.create(user=self.user2)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.delete('/rooms/1/')
        assert response.status_code == 401
        assert Room.objects.filter(id=1).exists()

    def test_delete_room_game_has_started(self):
        self.room.game_has_started = True
        self.room.save()
        response = self.client.delete('/rooms/1/')
        assert response.status_code == 409
        assert Room.objects.filter(id=1).exists()

    def test_delete_room_does_not_exist(self):
        assert not Room.objects.filter(id=13).exists()
        response = self.client.delete('/rooms/13/')
        assert response.status_code == 404


class RoomsCreateGameTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='Toshi',
                                             password='Come33veces!')
        self.user2 = User.objects.create_user(username='Pote',
                                              password='Come33veces!')
        self.user3 = User.objects.create_user(username='Hercules',
                                              password='Come33veces!')
        self.user4 = User.objects.create_user(username='Ernesto',
                                              password='Come33veces!')
        self.board = Board.objects.create(name="Reedwind")
        self.room = Room.objects.create(name='The Dragon room',
                                        owner=self.user,
                                        game_has_started=False,
                                        board=self.board)
        self.token = Token.objects.create(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_create_game_endpoint(self):
        users = [self.user, self.user4, self.user3, self.user2]
        for user in users:
            self.room.players.add(user)

        response = self.client.patch('/rooms/1/')
        assert response.status_code == 202
        assert Room.objects.filter(id=self.room.id,
                                   game_has_started=True).exists()

    def test_create_game_has_started(self):
        self.room.game_has_started = True
        self.room.save()
        response = self.client.patch('/rooms/1/')
        assert response.status_code == 409

    def test_create_game_players_invalid_number(self):
        users = [self.user, self.user2]
        for user in users:
            self.room.players.add(user)

        response = self.client.patch('/rooms/1/')
        assert response.status_code == 409

    def test_create_game_user_has_not_permission(self):
        self.token = Token.objects.create(user=self.user2)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.patch('/rooms/1/')
        assert response.status_code == 401
