from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.http import Http404
from catan.game_elements import RoomStatus
from django.contrib.auth.models import User
from board.models import Board


class Room(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(User,
                              null=True,
                              on_delete=models.CASCADE,
                              related_name='owner')
    players = models.ManyToManyField(User, related_name='players')
    max_players = models.PositiveSmallIntegerField(
        default=4,
        validators=[MinValueValidator(3), MaxValueValidator(4)]
    )
    game_has_started = models.BooleanField(default=False)
    board = models.ForeignKey(Board,
                              null=True,
                              on_delete=models.CASCADE,
                              related_name='board')
    game_id = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return "%s the room" % self.name

# Lo de abajo no debería estar acá, buscar lugar.


def build_room_model(request):
    owner = request.user
    room = Room.objects.create(
        name=get_name(request),
        owner=owner,
        board_id=get_board_id(request))
    room.players.add(owner)
    room.save()
    return room


def get_name(request):
    try:
        room_name = request.data["name"]
        return room_name
    except KeyError:
        return Response(status=status.HTTP_400_BAD_REQUEST)


def get_board_id(request):
    try:
        board_id = request.data["board_id"]
        board_exists(board_id)
        return board_id
    except KeyError:
        return Response(status=status.HTTP_400_BAD_REQUEST)


def board_exists(board_id):
    try:
        board = Board.objects.get(id=board_id)
    except Board.DoesNotExist:
        raise Http404


def room_exists(pk):
    try:
        room = Room.objects.get(pk=pk)
        return room
    except Room.DoesNotExist:
        raise Http404


def validate_room(room, username):
    valid_room = (not game_has_started(room) and
                  (number_players(room) < 4) and
                  player_does_not_exists(room, username))
    return valid_room


def game_has_started(room):
    return room.game_has_started


def number_players(room):
    return room.players.all().count()


def player_does_not_exists(room, username):
    return not room.players.filter(username=username).exists()
