from rest_framework import serializers

from room.models import Room


class RoomSerializer(serializers.ModelSerializer):
    owner = serializers.StringRelatedField()
    players = serializers.StringRelatedField(many=True)

    class Meta:
        model = Room
        fields = ['id',
                  'name',
                  'owner',
                  'players',
                  'max_players',
                  'game_has_started',
                  'game_id']
        read_only = fields
