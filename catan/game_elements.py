
class GameElements():
    BRICK = 'brick'
    LUMBER = 'lumber'
    WOOL = 'wool'
    GRAIN = 'grain'
    ORE = 'ore'

    R = 'road_building'
    Y = 'year_of_plenty'
    M = 'monopoly'
    V = 'victory_point'
    K = 'knight'
    A = 'great_army'
    C = 'commercial_route'

    RESOURCE = (
        (BRICK, 'brick'),
        (LUMBER, 'lumber'),
        (WOOL, 'wool'),
        (GRAIN, 'grain'),
        (ORE, 'ore')
    )

    CARD_TYPE = (
        (R, 'road_building'),
        (Y, 'year_of_plenty'),
        (M, 'monopoly'),
        (V, 'victory_point'),
        (K, 'knight'),
        (A, 'great_army'),
        (C, 'commercial_route')
    )


class RoomStatus():
    AVAILABLE = 'Available'
    PLAYING = 'Playing'

    STATUS = (
        (AVAILABLE, 'Available'),
        (PLAYING, 'Playing')
    )


class PlayerColour():
    R = 'red'
    B = 'blue'
    Y = 'yellow'
    W = 'white'

    COLOUR = (
        (R, 'red'),
        (B, 'blue'),
        (Y, 'yellow'),
        (W, 'white')
    )
