from django.db import models
from catan.game_elements import GameElements


class Resource(models.Model):
    resource = models.PositiveSmallIntegerField(choices=GameElements.RESOURCE)
    # player = models.ForeignKey(Player, on_delete=models.CASCADE,
    # related_name='resources')


class Card(models.Model):
    card_type = models.PositiveSmallIntegerField(
        choices=GameElements.CARD_TYPE)
    # player = models.ForeignKey(Player, on_delete=models.CASCADE,
    # related_name='cards')
